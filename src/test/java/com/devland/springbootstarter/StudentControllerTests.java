package com.devland.springbootstarter;

import java.util.List;

import com.devland.springbootstarter.student.Student;
import com.devland.springbootstarter.student.StudentRepository;
import com.devland.springbootstarter.student.StudentRequestDTO;
import com.devland.springbootstarter.student.StudentResponseDTO;
import com.devland.springbootstarter.student.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTests {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    StudentService studentService;

    @Autowired
    StudentRepository studentRepository;

    @AfterEach
    void cleanUp() {
        studentRepository.deleteAll();
    }

    @Test
    void getStudents_shouldReturnListOfStudentResponseDTO_whenInvoked() throws Exception {
        Student paksi = Student.builder().firstName("Paksi").lastName("Bumi").build();
        this.studentRepository.save(paksi);
        StudentResponseDTO studentResponseDTO = new StudentResponseDTO(paksi.getId(), paksi.getFirstName(), paksi.getLastName());
        List<StudentResponseDTO> studentResponseDTOs = List.of(studentResponseDTO);

        String expectedStudentList = this.objectMapper.writeValueAsString(studentResponseDTOs);
        RequestBuilder request = MockMvcRequestBuilders.get("/students");

        this.mockMvc.perform(request)
            .andExpect(MockMvcResultMatchers.content().json(expectedStudentList));
    }

    @Test
    void getStudentById_shouldReturnPaksi_whenGivenIdIs1() throws Exception {
        Student paksi = Student.builder().firstName("Paksi").lastName("Bumi").build();
        Student savedStudent = this.studentRepository.save(paksi);
        StudentResponseDTO studentResponseDTO = new StudentResponseDTO(savedStudent.getId(), savedStudent.getFirstName(), savedStudent.getLastName());
        
        String expectedStudent = this.objectMapper.writeValueAsString(studentResponseDTO);
        RequestBuilder request = MockMvcRequestBuilders.get("/students/" + savedStudent.getId());

        this.mockMvc.perform(request)
            .andExpect(MockMvcResultMatchers.content().json(expectedStudent));
    }

    @Test
    void createStudent_shouldReturnSavedStudent_whenGivenStudentRequestDTO() throws Exception {
        Student paksi = Student.builder().firstName("Paksi").lastName("Bumi").build();
        StudentRequestDTO studentRequestDTO = new StudentRequestDTO(paksi.getFirstName(),  paksi.getLastName());
        String requestBody = this.objectMapper.writeValueAsString(studentRequestDTO);
        
        RequestBuilder request = MockMvcRequestBuilders.post("/students")
                                .content(requestBody).contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(request)
                                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
        String responseJson = result.getResponse().getContentAsString();
        StudentResponseDTO actualStudent = this.objectMapper.readValue(responseJson, StudentResponseDTO.class);
        Student foundStudent = this.studentRepository.findById(actualStudent.getId()).get();
        StudentResponseDTO expectedStudent = new StudentResponseDTO(foundStudent.getId(), foundStudent.getFirstName(), foundStudent.getLastName());
        
        Assertions.assertThat(actualStudent).isEqualTo(expectedStudent);
    }

    @Test
    void updateStudent_shouldReturnUpdatedStudent_whenGivenStudentDataIsChanged() throws Exception{
        Student paksi = Student.builder().firstName("Paksi").lastName("Bumi").build();
        Student savedStudent = this.studentRepository.save(paksi);
        StudentRequestDTO studentRequestDTO = new StudentRequestDTO(paksi.getFirstName(), "Mega");
        String requestBody = this.objectMapper.writeValueAsString(studentRequestDTO);
        
        RequestBuilder request = MockMvcRequestBuilders.put("/students/" + savedStudent.getId())
                                .content(requestBody).contentType(MediaType.APPLICATION_JSON);
        this.mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isOk());
        Student actualStudent = this.studentRepository.findById(savedStudent.getId()).get();
        StudentResponseDTO actualResponse = new StudentResponseDTO(actualStudent.getId(), actualStudent.getFirstName(), actualStudent.getLastName());
        StudentResponseDTO expectedResponse = new StudentResponseDTO(actualStudent.getId(), actualStudent.getFirstName(), "Mega");

        Assertions.assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    void deleteStudent_shouldDeleteStudent_whenGivenStudentIdIsOne() throws Exception {
        Student paksi = Student.builder().firstName("Paksi").lastName("Bumi").build();
        Student savedStudent = this.studentRepository.save(paksi);

        RequestBuilder request = MockMvcRequestBuilders.delete("/students/" + savedStudent.getId());

        this.mockMvc.perform(request)
            .andExpect(MockMvcResultMatchers.status().isNoContent());

        Assertions.assertThat(this.studentRepository.findAll()).isEmpty();
    }
}
