package com.devland.springbootstarter;

import static org.mockito.Mockito.when;

import java.util.List;

import com.devland.springbootstarter.student.Student;
import com.devland.springbootstarter.student.StudentRepository;
import com.devland.springbootstarter.student.StudentService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudentServiceTests {

    @InjectMocks
    StudentService studentService;

    @Mock
    StudentRepository studentRepository;
}
