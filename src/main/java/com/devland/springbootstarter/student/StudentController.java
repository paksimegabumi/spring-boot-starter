package com.devland.springbootstarter.student;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.devland.springbootstarter.book.BookBorrowingHistoryRequestDTO;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity<List<StudentResponseDTO>> getStudents() {
        List<Student> studentList = this.studentService.getStudents();

        List<StudentResponseDTO> studentResponseDTOList = studentList.stream().map(
            student -> new StudentResponseDTO(
                student.getId(), student.getFirstName(), student.getLastName())
        ).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(studentResponseDTOList);
    }

    @GetMapping("/students/{studentId}")
    public ResponseEntity<StudentResponseDTO> getStudentById(@PathVariable("studentId") Long studentId) {
        Student student = this.studentService.findById(studentId);

        StudentResponseDTO studentResponseDTO = new StudentResponseDTO(student.getId(), student.getFirstName(), student.getLastName());
        
        return ResponseEntity.status(HttpStatus.OK).body(studentResponseDTO);
    }

    @PostMapping("/students")
    public ResponseEntity<StudentResponseDTO> createStudent(@Valid @RequestBody StudentRequestDTO studentRequestDTO) {
        Student newStudent = new Student();
        newStudent.setFirstName(studentRequestDTO.getFirstName());
        newStudent.setLastName(studentRequestDTO.getLastName());

        Student savedStudent = this.studentService.createStudent(newStudent);
        StudentResponseDTO studentResponseDTO = new StudentResponseDTO(savedStudent.getId(), savedStudent.getFirstName(), savedStudent.getLastName());

        return ResponseEntity.status(HttpStatus.CREATED).body(studentResponseDTO);
    }

    @PutMapping(value="/students/{studentId}")
    public ResponseEntity<StudentResponseDTO> updateStudent(@PathVariable Long studentId, @RequestBody StudentRequestDTO studentRequestDTO) {
        Student student = new Student();
        student.setFirstName(studentRequestDTO.getFirstName());
        student.setLastName(studentRequestDTO.getLastName());

        Student updatedStudent = this.studentService.updateStudent(studentId, student);
        StudentResponseDTO studentResponseDTO = new StudentResponseDTO(updatedStudent.getId(), updatedStudent.getFirstName(), updatedStudent.getLastName());
        
        return ResponseEntity.status(HttpStatus.OK).body(studentResponseDTO);
    }

    @DeleteMapping(value = "/students/{studentId}")
    public ResponseEntity<Void> deleteStudent(@PathVariable Long studentId) {
        this.studentService.deleteStudent(studentId);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/students/{studentId}/borrowings")
    public ResponseEntity<Void> createBorrowingBooks(@PathVariable("studentId") Long studentId, @RequestBody BookBorrowingHistoryRequestDTO bookBorrowingHistoryRequestDTO) {
        this.studentService.createStudentBorrow(studentId, bookBorrowingHistoryRequestDTO);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
