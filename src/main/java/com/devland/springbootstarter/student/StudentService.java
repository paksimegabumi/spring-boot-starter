package com.devland.springbootstarter.student;

import java.util.List;
import java.util.Optional;

import com.devland.springbootstarter.book.BookBorrowingHistoryRequestDTO;
import com.devland.springbootstarter.book.BookBorrowingHistoryService;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Service
public class StudentService {
    private final StudentRepository studentRepositories;
    private final BookBorrowingHistoryService bookBorrowingHistoryService;

    public List<Student> getStudents() {
        log.info("Fetching Students");
        return this.studentRepositories.findAll();
    }

    public Student findById(Long studentId) {
        log.info("Fetching Student by id:" + studentId);
        Optional<Student> existingStudent = this.studentRepositories.findById(studentId);

        if(existingStudent.isEmpty()) {
            log.error("Error " +  new StudentNotFoundException().getLocalizedMessage());
            throw new StudentNotFoundException();
        }
        
        return existingStudent.get();
    }

    public Student createStudent(Student student) {
        log.info("Creating Student");
        return this.studentRepositories.save(student);
    }

    public Student updateStudent(Long studentId, Student student) {
        log.info("Updating Students");
        Student existingStudent = findById(studentId);

        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setLastName(student.getLastName());
        existingStudent.setId(studentId);
        return this.studentRepositories.save(existingStudent);
    }

    public void deleteStudent(Long studentId) {
        log.info("Delete Student by id: " + studentId);
        Student existingStudent = findById(studentId);

        this.studentRepositories.delete(existingStudent);
    }

    public void createStudentA(StudentRequestDTO StudentRequestDTO) {
        Student newStudent = new Student();
        newStudent.setFirstName(StudentRequestDTO.getFirstName());
        newStudent.setLastName(StudentRequestDTO.getLastName());
    }

    public void createStudentBorrow(Long studentId, BookBorrowingHistoryRequestDTO bookBorrowingHistoryRequestDTO) {
        Student student = findById(studentId);
        this.bookBorrowingHistoryService.createBookBorrowingHistory(student, bookBorrowingHistoryRequestDTO);
    }
}
