package com.devland.springbootstarter.book;

import java.time.LocalDateTime;

import com.devland.springbootstarter.student.Student;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class BookBorrowingHistoryService {
    private final BookService bookService;
    private final BookBorrowingHistoryRepository bookBorrowingHistoryRepository;

    public void createBookBorrowingHistory(Student student,
            BookBorrowingHistoryRequestDTO bookBorrowingHistoryRequestDTO) {
        Book book = this.bookService.findById(bookBorrowingHistoryRequestDTO.getBookId());
        LocalDateTime currentDateTime = LocalDateTime.now();
        
        BookBorrowingHistory newBookBorrowingHistory = BookBorrowingHistory.builder().student(student).book(book).borrowedAt(currentDateTime).build();
        this.bookService.bookBorrows(book);

        this.bookBorrowingHistoryRepository.save(newBookBorrowingHistory);
    }

}
